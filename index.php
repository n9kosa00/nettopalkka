<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nettopalkka</title>
</head>
<style>
    #forminput {
        padding: 5px;
        width: 500px;
    }
    .right {
        float: right;
    }
</style>
<body>
    <h3>Nettopalkka</h3>
    <form action="index.php" method="POST">
        <div id="forminput">
            <label>Bruttopalkka €</label>
            <input class="right" name="brutto" type="number" step="0.01" required>
        </div>
        
        <div id="forminput">
            <label>Ennakkopidätys %</label>
            <input class="right" name="ennakko" type="number" step="0.01" required>
        </div>
        
        <div id="forminput">
            <label>Työeläkemaksu %</label>
            <input class="right" name="tyel" type="number" step="0.01" required>
        </div>
        
        <div id="forminput">
            <label>Työttömyysvakuutusmaksu %</label>
            <input class="right" name="tvm" type="number" step="0.01" required>
        </div>
        
        <button>Laske</button>
    </form>

    <?php 
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $brutto = filter_input(INPUT_POST,'brutto',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $ennakkopros = filter_input(INPUT_POST,'ennakko',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tyelpros = filter_input(INPUT_POST,'tyel',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tvmpros = filter_input(INPUT_POST,'tvm',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $netto = 0;
    
        $ennakko = $brutto / 100 * $ennakkopros;
        $tyel = $brutto / 100 * $tyelpros;
        $tvm = $brutto / 100 * $tvmpros;
    
        $netto = $brutto - ($ennakko + $tyel + $tvm);
    
        printf("<p>Bruttopalkka: %.2f€</p>",$brutto);
        printf("<p>Ennakkopidatys: %.2f€</p>",$ennakko);
        printf("<p>Työeläkemaksu: %.2f€</p>",$tyel);
        printf("<p>Työttömyysvakuutusmaksu: %.2f€</p>",$tvm);
        printf("<hr>");
        printf("<p>Nettopalkka: %.2f€</p>",$netto);
    }
    
    ?>

</body>
</html>